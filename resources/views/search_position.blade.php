<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<style>
    .section-form{
        margin-left: auto;
        margin-right: auto;
        width: 100%;
    }

    form{
        position: absolute;
        top: 50%; left: 50%;
        transform: translate(-50%, -50%);
    }

    .section-map{
        margin-top: 30px;
    }

    .section-map h1{
        color: white ;
        border: 1px solid #87eda3;
        border-radius: 15px;
        background-color: #43a35d;
        padding: 20px;
        box-shadow:  5px 5px #bec4c0;
        text-transform: uppercase;
    }
    .section-map h5{
        margin-top: 30px;
    }
</style>
<body>
<div class="container">
    <section class="section-map">
        <div class="row">
            <h1 class="text-center">Bienvenue sur l'application de recherche de poubelle</h1>
            <h5 class="text-center">Trouver le chemin le plus court pour accéder à une poubelle</h5>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p>La carte map serait ici</p>
            </div>
        </div>
    </section>
    <div class="section-form">
        <div class="">
            <form action="">
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Adresse de la poubelle*</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Numéro poubelle">
                </div >
                <div class="mb-3 text-center">
                    <button class="btn btn-primary btn-lg">Rechercher la position</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>
